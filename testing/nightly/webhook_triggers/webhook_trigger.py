#! /usr/bin/python3
import requests
import webhook_payloads


def trigger_webhook(webhook):
    try:
        r = requests.post(
            url=webhook.endpoint, data=webhook.data, headers=webhook.header
        )
        r.raise_for_status()
        print(f"request triggered for {webhook.header}")
    except requests.exceptions.HTTPError as err:
        raise SystemExit(err)


for payload in webhook_payloads.payloads:
    trigger_webhook(payload)
