default:
  tags: [automotive-runner]
  image: quay.io/automotive-toolchain/manifest-tools:latest

stages:
  - lint
  - build
  - deploy

before_script:
  - cd ./deployment

start-pipelinerun:
  stage: build
  script:
    - ansible-playbook -v start-pipelinerun.yml
      -e "env=upstream_deployment_vars
      developer=true
      secret_path=apps/data/automotive/upstream/stage
      ci_revision=${CI_COMMIT_SHA}
      GitRevision=${CI_COMMIT_SHA}
      GitMergeIid=${CI_MERGE_REQUEST_IID}
      GitSourceProjectId=${CI_MERGE_REQUEST_SOURCE_PROJECT_ID}
      GitTargetProjectId=${CI_MERGE_REQUEST_SOURCE_PROJECT_ID}
      pipelinerun_name=pipelines-as-code-${CI_MERGE_REQUEST_IID}-${RANDOM}"
  environment:
    name: upstream/stage
  only:
    - merge_requests

start-productbuildrun:
  stage: build
  script:
    - ansible-playbook -v start-productbuildrun.yml
      -e "env=upstream_deployment_vars
      developer=true
      secret_path=apps/data/automotive/upstream/stage
      ci_revision=${CI_COMMIT_SHA}
      GitRevision=${CI_COMMIT_SHA}
      GitCommitAfter=${CI_COMMIT_SHA}
      GitCommitBefore=${CI_COMMIT_BEFORE_SHA}
      GitMergeIid=${CI_MERGE_REQUEST_IID}
      GitSourceProjectId=${CI_MERGE_REQUEST_SOURCE_PROJECT_ID}
      GitTargetProjectId=${CI_MERGE_REQUEST_SOURCE_PROJECT_ID}
      pipelinerun_name=pipelines-as-code-product-build-${CI_MERGE_REQUEST_IID}-${RANDOM}"
  environment:
    name: upstream/stage
  only:
    - merge_requests

deploy_stage:
  stage: deploy
  script:
    - ansible-playbook main.yml
      -e "env=upstream_deployment_vars
      secret_path=apps/data/automotive/upstream/stage
      ci_revision=${CI_COMMIT_SHA}"
    - ansible-playbook publish-product-build.yml
      -e "env=upstream_deployment_vars
      secret_path=apps/data/automotive/upstream/stage
      ci_revision=${CI_COMMIT_SHA}"
  environment:
    name: upstream/stage
  when: manual

deploy_prod:
  stage: deploy
  script:
    - ansible-playbook main.yml -e "env=upstream_deployment_vars
      secret_path=apps/data/automotive/upstream/prod
      ci_revision=${CI_COMMIT_SHA}"
    - ansible-playbook publish-product-build.yml -e "env=upstream_deployment_vars
      secret_path=apps/data/automotive/upstream/prod
      ci_revision=${CI_COMMIT_SHA}"
  environment:
    name: upstream/prod
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual

deploy_downstream_stage:
  stage: deploy
  script:
    - ansible-playbook downstream.yml
      -e "env=downstream_deployment_vars
      secret_path=apps/data/automotive/downstream/stage
      ci_revision=${CI_COMMIT_SHA}"
    - ansible-playbook publish-product-build.yml
      -e "env=downstream_deployment_vars
      secret_path=apps/data/automotive/downstream/stage
      ci_revision=${CI_COMMIT_SHA}"
  environment:
    name: downstream/stage
  when: manual

deploy_downstream_prod:
  stage: deploy
  script:
    - ansible-playbook downstream.yml -e "env=downstream_deployment_vars
      secret_path=apps/data/automotive/downstream/prod"
    - ansible-playbook publish-product-build.yml -e "env=downstream_deployment_vars
      secret_path=apps/data/automotive/downstream/prod"
  environment:
    name: downstream/prod
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual

tmt-check:
  image: quay.io/testing-farm/tmt:latest
  stage: lint
  before_script:
    - tmt --version
  script:
    - tmt lint .
    - tmt plans ls
  # only run check on changes in merge requests, on specified files
  only:
    refs:
      - merge_requests
    changes:
      - "ci/**/*.fmf"
  allow_failure: true

shell-check:
  image: koalaman/shellcheck-alpine:stable
  stage: lint
  before_script:
    - shellcheck --version
  script: # TODO should also check for warnings before merge to main
    - find -name '*.sh' -exec shellcheck --severity=error -f gcc {} +
  # only run check on changes in merge requests, on specified files
  only:
    refs:
      - merge_requests
    changes:
      - "**/*.sh"
  allow_failure: true

ansible-check:
  image: quay.io/ansible/toolset:latest
  stage: lint
  before_script:
    - ansible-lint --version
  script:
    - ansible-lint -x var-naming deployment
  # only run check on changes in merge requests, on specified files
  only:
    refs:
      - merge_requests
    changes:
      - "deployment/**/*.yml"
  allow_failure: false

python-check:
  image: registry.access.redhat.com/ubi8/python-39:1-15
  stage: lint
  before_script:
    - pip install --upgrade pip
    - pip install pylint
    - pylint --version
  script:
    # install modules used by the scripts to avoid mis-reporting
    - pip install -r ./deployment/requirements.txt
    - pip install -r ./ci/create_yum_repo/requirements.txt
    - find -name '*.py' -exec pylint {} +
  # only run check on changes in merge requests, on specified files
  only:
    refs:
      - merge_requests
    changes:
      - "**/*.py"
  allow_failure: true
