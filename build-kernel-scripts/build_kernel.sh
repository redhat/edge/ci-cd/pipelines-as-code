#!/bin/bash
set -eo pipefail

[ $# != 1 ] && echo "Usage: $0 <Kernel Source URL>" && exit 1

URL=$1
echo ${URL} | grep -q "^https://.*\.tar\.gz"

FN=$(echo ${URL}| rev |cut  -d"/" -f1|rev)

mkdir -p src && cd src

echo "getting kernel source ${FN} ..."
wget "${URL}" -q

echo "Decompressing ${FN} ..."
DIR=$(tar -tf ${FN} | cut -d"/" -f1| uniq)
tar zxf ${FN}

echo "Build prepare..."
cd ${DIR}
git config --global user.email "noname@redhat.com"
git config --global user.name "noname"
git init &>/dev/null
make dist-configs &>/dev/null
HW=$(uname -m)
cp redhat/configs/*${HW}.config .config
make oldconfig &>/dev/null

START_TS=$(date "+%s")
echo "Building... (may take long time)"
make -j $(nproc) &>/dev/null
END_TS=$(date "+%s")
echo "Building... Done in $((END_TS - START_TS))"
