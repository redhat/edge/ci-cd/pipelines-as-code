---
- name: A-Team Pipeline | k8s | Validate vars are provided
  ansible.builtin.assert:
    that:
      - vars[item] != None
  with_items:
    - ocp_host
    - ocp_ns
    - ocp_key
    - ocp_pipeline_key
    - ci_repo_url
    - ci_revision
    - ci_repos_endpoint
    - ci_images_endpoint
    - ci_logs_endpoint
    - gitlab_token
    - gitlab_key
    - gitlab_host_url
    - aws_access_key_id
    - aws_secret_access_key
    - aws_region
    - aws_tf_region
    - aws_tf_account_id
    - aws_bucket_repos
    - aws_bucket_logs
    - aws_product_build_bucket_repos
    - stream
    - ssh_private_key
    - os_prefix
    - product_build_prefix
    - tf_os_options
    - tf_distro_name
    - tf_format
    - tf_api_key
    - tf_ssh_key
    - qe_ssh_key
    - ps_ssh_key
    - tf_compose
    - ssl_verify
    - fusa_service_gitlab_token
    - google_api_token
    - google_api_key
    - fusa_req_governance_doc_url
    - jira_host
    - jira_token
    - jira_report_issues
    - jira_report_traced_issues

- name: A-Team Pipeline | k8s | Create Tooling Container ImageStream
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: image.openshift.io/v1
      kind: ImageStream
      metadata:
        name: create_pipeline
        namespace: "{{ ocp_ns }}"
        annotations:
          openshift.io/display-name: create_pipeline
      spec:
        tags:
          - name: latest
        lookupPolicy:
          local: true

- name: A-Team Pipeline | k8s | Create Tooling Container BuildConfig
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: build.openshift.io/v1
      kind: BuildConfig
      metadata:
        name: create-pipeline-tooling
        namespace: "{{ ocp_ns }}"
      spec:
        runPolicy: Serial
        triggers:
          - type: "GitLab"
            gitlab:
              secret: "{{ gitlab_token }}"
          - type: "ConfigChange"
        source:
          git:
            uri: "https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code.git"
            ref: main
          contextDir: ./deployment/
        strategy:
          type: Docker
          dockerStrategy:
            env:
              - name: GIT_SSL_NO_VERIFY
                value: "true"
            dockerfilePath: Containerfile
        output:
          to:
            kind: "ImageStreamTag"
            name: "create_pipeline:latest"

- name: A-Team Pipeline | k8s | Create create_yum_repo Container ImageStream
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: image.openshift.io/v1
      kind: ImageStream
      metadata:
        name: create_yum_repo
        namespace: "{{ ocp_ns }}"
        annotations:
          openshift.io/display-name: create_yum_repo
      spec:
        tags:
          - name: latest
        lookupPolicy:
          local: true

- name: A-Team Pipeline | k8s | Create create_yum_repo Container BuildConfig
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: build.openshift.io/v1
      kind: BuildConfig
      metadata:
        name: create-yum-repo-tooling
        namespace: "{{ ocp_ns }}"
      spec:
        resources:
          requests:
            memory: 256Mi
          limits:
            memory: 1Gi
        runPolicy: Serial
        triggers:
          - type: "GitLab"
            gitlab:
              secret: "{{ gitlab_token }}"
          - type: "ConfigChange"
        source:
          git:
            uri: "https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code.git"
            ref: main
          contextDir: ./ci/create_yum_repo
        strategy:
          type: Docker
          dockerStrategy:
            env:
              - name: GIT_SSL_NO_VERIFY
                value: "true"
            dockerfilePath: Containerfile
        output:
          to:
            kind: "ImageStreamTag"
            name: "create_yum_repo:latest"

- name: A-Team Pipeline | k8s | Create Certification Assembly Container ImageStream
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: image.openshift.io/v1
      kind: ImageStream
      metadata:
        name: certification_assembly
        namespace: "{{ ocp_ns }}"
        annotations:
          openshift.io/display-name: certification_assembly
      spec:
        tags:
          - name: latest
        lookupPolicy:
          local: true

- name: A-Team Pipeline | k8s | Create Certification Assembly Container BuildConfig
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: build.openshift.io/v1
      kind: BuildConfig
      metadata:
        name: certification-assembly
        namespace: "{{ ocp_ns }}"
      spec:
        resources:
          requests:
            memory: 256Mi
          limits:
            memory: 512Mi
        runPolicy: Serial
        triggers:
          - type: "GitLab"
            gitlab:
              secret: "{{ fusa_service_gitlab_token }}"
          - type: "ConfigChange"
        source:
          git:
            uri: "https://gitlab.com/redhat/edge/ci-cd/a-team/fusa-snapshot.git"
            ref: main
          sourceSecret:
            name: "pipeline-token"
        strategy:
          type: Docker
          dockerStrategy:
            env:
              - name: GIT_SSL_NO_VERIFY
                value: "true"
            dockerfilePath: Containerfile
        output:
          to:
            kind: "ImageStreamTag"
            name: "certification_assembly:latest"
- name: A-Team Pipeline | k8s | Create Nightly Test Container ImageStream
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: image.openshift.io/v1
      kind: ImageStream
      metadata:
        name: nightly_webhook_trigger
        namespace: "{{ ocp_ns }}"
        annotations:
          openshift.io/display-name: nightly_webhook_trigger
      spec:
        tags:
          - name: latest
        lookupPolicy:
          local: true

- name: A-Team Pipeline | k8s | Create Nightly Trigger Container BuildConfig
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: absent
    definition:
      apiVersion: build.openshift.io/v1
      kind: BuildConfig
      metadata:
        name: create-nightly-webhook-trigger
        namespace: "{{ ocp_ns }}"
      spec:
        resources:
          requests:
            memory: 256Mi
          limits:
            memory: 1Gi
        runPolicy: Serial
        triggers:
          - type: "GitLab"
            gitlab:
              secret: "{{ gitlab_token }}"
          - type: "ConfigChange"
        source:
          git:
            uri: "https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code.git"
            ref: main
          contextDir: ./testing/nightly/webhook_triggers/
        strategy:
          type: Docker
          dockerStrategy:
            env:
              - name: GIT_SSL_NO_VERIFY
                value: "true"
            dockerfilePath: Containerfile
        output:
          to:
            kind: "ImageStreamTag"
            name: "nightly_webhook_trigger:latest"

- name: A-Team Pipeline | k8s | Create Nightly Trigger Container BuildConfig
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: build.openshift.io/v1
      kind: BuildConfig
      metadata:
        name: create-nightly-webhook-trigger
        namespace: "{{ ocp_ns }}"
      spec:
        resources:
          requests:
            memory: 256Mi
          limits:
            memory: 1Gi
        runPolicy: Serial
        triggers:
          - type: "GitLab"
            gitlab:
              secret: "{{ gitlab_token }}"
          - type: "ConfigChange"
        source:
          git:
            uri: "https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code.git"
            ref: main
          contextDir: ./testing/nightly/webhook_triggers/
        strategy:
          type: Docker
          dockerStrategy:
            env:
              - name: GIT_SSL_NO_VERIFY
                value: "true"
            dockerfilePath: Containerfile
        output:
          to:
            kind: "ImageStreamTag"
            name: "nightly_webhook_trigger:latest"

- name: A-Team Pipeline | k8s | Secret
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: secure-props
        namespace: "{{ ocp_ns }}"
      type: Opaque
      data:
        GITLAB_TOKEN: "{{ gitlab_token | b64encode }}"
        GITLAB_KEY: "{{ gitlab_key | b64encode }}"
        AWS_ACCESS_KEY_ID: "{{ aws_access_key_id | b64encode }}"
        AWS_SECRET_ACCESS_KEY: "{{ aws_secret_access_key | b64encode }}"
        SSH_PRIVATE_KEY: "{{ ssh_private_key | b64encode }}"
        TF_API_KEY: "{{ tf_api_key | b64encode }}"
        NIGHTLY_WEBHOOK_ENDPOINT: "{{ nightly_webhook_endpoint | b64encode }}"
        NIGHTLY_GIT_SSH_URL: "{{ nightly_git_ssh_url | b64encode }}"
        NIGHTLY_GIT_HTTP_URL: "{{ nightly_git_http_url | b64encode }}"
        NIGHTLY_GIT_PROJECT_ID: "{{ nightly_git_project_id | b64encode }}"
        GOOGLE_API_KEY: "{{ google_api_key | b64encode }}"
        GOOGLE_API_TOKEN: "{{ google_api_token | b64encode }}"
        FUSA_REQ_GOVERNANCE_DOC_URL: "{{ fusa_req_governance_doc_url | b64encode }}"
        JIRA_HOST: "{{ jira_host | b64encode }}"
        JIRA_TOKEN: "{{ jira_token | b64encode }}"
        JIRA_REPORT_ISSUES: "{{ jira_report_issues | b64encode }}"
        JIRA_REPORT_TRACED_ISSUES: "{{ jira_report_traced_issues | b64encode }}"

# Create a separate secret for the CA cert to avoid mounting every secret to tekton git-clone tasks
- name: A-Team Pipeline | k8s | CA-cert Secret
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: ca-certs
        namespace: "{{ ocp_ns }}"
      type: Opaque
      data:
        ca.crt: "{{ ca_cert | b64encode }}"
        ca.pem: "{{ ca_cert | b64encode }}"

- name: A-Team Pipeline | k8s | Create Nightly Cronjob
  kubernetes.core.k8s:
    api_key: "{{ ocp_key }}"
    host: "{{ ocp_host }}"
    validate_certs: "{{ validate_certs }}"
    state: present
    definition:
      apiVersion: batch/v1
      kind: CronJob
      metadata:
        name: nightly-job
        namespace: "{{ ocp_ns }}"
      spec:
        schedule: "0 3 */1 * *"
        jobTemplate:
          spec:
            template:
              spec:
                containers:
                  - name: nightly-webhook-trigger
                    image: nightly_webhook_trigger:latest
                    imagePullPolicy: Always
                    envFrom:
                      - secretRef:
                          name: secure-props
                    command:
                      - /bin/sh
                      - -c
                      - python3 /usr/bin/webhook_trigger.py
                restartPolicy: Never
