# Deployment

The **A-Team pipeline** uses **OpenShift Pipelines** ([Tekton]) for building
and testing the different images.

The pipeline is defined as a set of tasks linked together in a specific
order. Some tasks will run in sequence, some in parallel.

For the integration with [Gitlab], the project uses a [webhook] which will
trigger the pipeline each time a developer files a MR (Merge Request), in
addition it can also be triggered using Tekton CLI ([tkn]).

Currently MR (Merge Request) from a source git fork are not supported.
MR from a source git branch are supported.

Merge request is also proposed by [rome](https://gitlab.com/redhat/edge/ci-cd/a-team/rome) via [dover](https://gitlab.com/redhat/edge/ci-cd/a-team/dover) to sync with the package updates from the upstream *cs9 (CentOS stream 9)*.

## Manifest-tools containerfile
[![Docker Repository on Quay](https://quay.io/repository/automotive-toolchain/manifest-tools/status "Docker Repository on Quay")](https://quay.io/repository/automotive-toolchain/manifest-tools)

To build this container, you need to download the needed CA certificate into the deployment directory as RH-IT-Root-CA.crt.

## Installation

- Install the required modules listed in the `deployment/requirements.txt`
- Download and install [OpenShift CLI](https://docs.openshift.com/container-platform/4.8/cli_reference/openshift_cli/getting-started-cli.html)

## Prerequisite

- Login to the cluster
  - Using the `oc login` command retrieved from the openshift cluster

## Deploy the pipeline

The pipelines are deployed using [Ansible] and all the necessary files are in
this (`deployment`) directory. It's recommended to use the automated process.

### Deploy stage environment

Execute GitLab [Run Pipeline](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/pipelines/new)
on any branch, then start `deploy_stage`.

It is possible to deploy a specific tag by setting the CI_COMMIT_SHA variable. 
e.g. Variable: `CI_COMMIT_SHA` `v0.02`
then start `deploy_stage`


### Deploy prod environment

Execute GitLab [Run Pipeline](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/pipelines/new)
on the `main` branch, then start `deploy_prod`.

It is possible to deploy a specific tag by setting the CI_COMMIT_SHA variable. 
e.g. Variable: `CI_COMMIT_SHA` `v0.02`
then start `deploy_prod`

## Development guide

### Add new tasks to the pipeline

The pipeline tasks are defined at the file: [tasks/tasks.yml](tasks/tasks.yml).
Any change to the current tasks or the addition to new task should be made
there.

To understand better how the **tasks** work, it is recomended to look at the
upstream documentation for the [Tekton Tasks].

Then the task should be added to the pipeline at this file:
[tasks/pipelinespec.yml](tasks/pipelinespec.yml). Here is the upstream documentation
for the [Tekton Pipelines].

The format is different from the examples on that documentation. 
That's because this project uses Ansible for generating all those resources.

[Tekton]: https://tekton.dev/
[Gitlab]: https://gitlab.com
[webhook]: https://docs.gitlab.com/ee/user/project/integrations/webhooks.html
[tkn]: https://tekton.dev/docs/cli/
[Ansible]: https://docs.ansible.com/
[Tekton Tasks]: https://github.com/tektoncd/pipeline/blob/main/docs/tasks.md
[Tekton Pipelines]: https://github.com/tektoncd/pipeline/blob/main/docs/pipelines.md

### Add new parameters to the pipeline

The pipeline parameters are defined at the file: [tasks/parameters.yml](tasks/parameters.yml).
Any change to the current parameters or the addition to new parameters should be made
there.

The parameters are defined into different blocks:
- params_hook --> params forwarded by the hook trigger 
- params_default --> default params for pipeline and taskspec
- params_task --> shared set of params for all tasks 
- params_env --> params converted into env variables for all tasks

The dedicated task parameters are set under each task on [tasks/pipelinespec.yml](tasks/pipelinespec.yml)

e.g. [params_create_osbuild_cs9_x86_64]

We can see below how shared params and dedicated params are set under [tasks/tasks.yml](tasks/tasks.yml) 

e.g. [pipelinespec params_task + dedicated params]

[pipelinespec params_task + dedicated params]: https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/blob/24ee60a113a157d6cef86588a727c1634a246f73/deployment/tasks/pipelinespec.yml#L128

[params_create_osbuild_cs9_x86_64]: https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/blob/24ee60a113a157d6cef86588a727c1634a246f73/deployment/tasks/tasks.yml#L344

### Pipeline yamls scope

The pipeline definition is assembled from multiple yaml files:
- [tasks/pipelinespec.yml](tasks/pipelinespec.yml) --> core pipeline definition 
- [tasks/pipelinerun.yml](tasks/pipelinerun.yml) --> PipelineRun entrypoint, it allows to start the pipeline from [.gitlab-ci.yml](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/blob/main/.gitlab-ci.yml)
- [tasks/pipeline.yml](tasks/pipeline.yml) --> Pipeline reference, it allows to start the pipeline from the hook triggers

### webhook vs gitlab-ci

Development of [pipelines-as-code](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code) leverages [.gitlab-ci.yml](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/blob/main/.gitlab-ci.yml). The hook parameters are injected into the PipelineRun.

Development of the application ingress [tasks/webhook.yml](tasks/wehook.yml) may be tested from the [automotive-sig-test](https://gitlab.com/redhat/edge/ci-cd/auto-sig-test) repo.

### Testing Farm integration

Pipelines-as-code uses [Testing Farm](https://gitlab.com/testing-farm) for executing tests. The integration between pipelines-as-code and Testing Farm is done under [testing-farm-request.sh](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/blob/24ee60a113a157d6cef86588a727c1634a246f73/ci/common/testing-farm-request.sh)

The pipeline parameters are bound into the above REST API request.

### Environments and secrets 

Pipelines-as-code environments and secrets are provisioned with Gitlab-CI + Ansible + Vault. The Environments are set under [.gitlab-ci.yml](https://gitlab.com/redhat/edge/ci-cd/pipelines-as-code/-/blob/main/.gitlab-ci.yml), secrets are fetched from Vault. 


#### Webhook Deployment

- Create or edit an existing webhook on a Git repository

    example URL: `http://a-team-<namespace>.apps.<cluster-name>.openshiftapps.com`

    *note: the `a-team` prefix is the name of the event listener route, which may differ depending on your deployment and setup*

- Add `push` events to the trigger rules referring to your main branch


### Downloading product-build

To download product-build run the following command:

***pre-req: valid AWS credentials in the user environment:***

```sh
    export AWS_ACCESS_KEY_ID=<your-aws-access-key-id>
    export AWS_SECRET_ACCESS_KEY=<your-aws-secret-access-key>
```

```sh
    aws s3 cp s3://auto-product-build/product-build-<timestamp>/cs9 <your-desired-folder> --recursive
```

Or

***Note: 'latest' prefix tends to be unavailable at some point during the build time***
```sh
    aws s3 cp s3://auto-product-build/latest/cs9 <your-desired-folder> --recursive
```
