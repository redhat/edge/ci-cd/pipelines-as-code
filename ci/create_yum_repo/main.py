#!/usr/bin/env python3

import logging
import logging.config
import os
import sys
import json
import tempfile
from concurrent import futures
from pathlib import Path

import koji
import shutil
import boto3
import requests
from jinja2 import Template
from requests.adapters import HTTPAdapter
import datetime
import re
import utils
import time

def upload_directory(directory: str, prefix: str, bucket: str) -> int:
    ret = 0
    boto3_session = boto3.Session()
    s3 = boto3_session.client("s3")

    input_dir = Path(directory)

    s3_parallel_request_rate = 16
    with futures.ThreadPoolExecutor(s3_parallel_request_rate) as executor:
        upload_task = {}

        for file in input_dir.rglob("*"):
            if not file.is_file():
                continue

            extra_args = {}
            if file.name.endswith(".html"):
                extra_args["ContentType"] = "text/html"

            upload_task[
                executor.submit(
                    s3.upload_file,
                    Filename=str(file.absolute()),
                    Bucket=bucket,
                    Key=prefix + "/" + str(file.relative_to(input_dir)),
                    ExtraArgs=extra_args,
                )
            ] = file

        for task in futures.as_completed(upload_task):
            try:
                task.result()
            except Exception as e:
                logging.error(
                    f"Exception {e} encountered while uploading file"
                    f"{upload_task[task]}"
                )
                ret = 1
    return ret


def create_index_html(topdir: str) -> int:
    return utils.run_cmd(cmd=["tree", "-H", ".", topdir, "-o", f"{topdir}/index.html"])


def create_repo(repodir: str) -> int:
    return utils.run_cmd(cmd=["createrepo_c", repodir])


def test_repo(baseurl: str, arch: str) -> int:
    """Call dnf makecache to test the created repos are valid"""
    logging.info(f"Testing dnf repo at {baseurl} for {arch}")

    # Configure and call dnf using a custom dnf.conf to avoid contaminating the host system
    dnfconf_template_file = Path(__file__).parent / "dnf.conf.j2"
    repofile_template_file = Path(__file__).parent / "yumrepo.j2"

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)
        (tmpdir / "cache").mkdir(parents=True)
        (tmpdir / "logs").mkdir(parents=True)
        (tmpdir / "persist").mkdir(parents=True)
        (tmpdir / "repos").mkdir(parents=True)

        dnfconf = Template(dnfconf_template_file.read_text()).render(tmpdir=tmpdirname)
        (tmpdir / "dnf.conf").write_text(dnfconf)

        repofile = Template(repofile_template_file.read_text()).render(
            arch=arch, baseurl=baseurl
        )
        (tmpdir / "repos" / "automotive.repo").write_text(repofile)
        logging.info(repofile)

        result = utils.run_cmd(
            cmd=[
                "dnf",
                "-v",
                "-c",
                f"{tmpdirname}/dnf.conf",
                "makecache",
                "--repo=auto",
            ]
        )
        return result


def test_remote_repo(
    repos_endpoint: str, upload_prefix: str, centos_version: int, arch: str
) -> int:
    """Call dnf makecache to test the uploaded repos are valid"""
    baseurl = f"{repos_endpoint}/{upload_prefix}/{centos_version}"

    return test_repo(baseurl, arch)


def test_local_repo(repo_dir: str, centos_version: int, arch: str) -> int:
    """Call dnf makecache to test the local repos are valid"""
    baseurl = f"file://{repo_dir}/{centos_version}"

    return test_repo(baseurl, arch)


def download_file(http_session: requests.Session, url: str, download_dir: str) -> int:
    resp = http_session.get(url=url, allow_redirects=True, timeout=60)
    if resp.status_code == 200:
        # workaround: S3 doesn't like the character '+' at the URL (PATH or file names)
        # It's a known issue:
        # https://stackoverflow.com/questions/36734171/how-to-decide-if-the-filename-has-a-plus-sign-in-it
        # The next line will rename the files with '+' with '-'. This is done before the
        # 'createrepo_c' indexes the packages, so the repo's metadata will point
        # to the files with the new names. dnf/yum and the osbuild can now download
        # and install the packages.
        package_filename = os.path.basename(url)
        package_filename = package_filename.replace("+", "-")
        with open(f"{download_dir}/{package_filename}", "wb") as package_file:
            package_file.write(resp.content)
        resp.close()
        return 0

    return 1


def find_koji_package(koji_session: koji.ClientSession, package: str, arch: str) -> str:
    find_build = koji_session.getRPM(f"{package}.{arch}")
    if not find_build:
        return

    find_package = koji_session.getBuild(find_build["build_id"])
    if not find_package:
        return
    package_name = find_package["name"]
    # Seemingly no API to determine these
    if package_name == "nspr":
        package_name = "nss"
    return package_name

def download_package(
    package: str,
    output_dir: str,
    mirrors: list,
    koji_mirrors: list,
    repos: list,
    package_arch: str,
    http_session: requests.Session,
    centos_version: str,
) -> int:
    supported_arches = {
        "x86_64": ["x86_64", "noarch"],
        "aarch64": ["aarch64", "noarch"],
    }

    download_dir = f"{output_dir}/{package_arch}/os/Packages"

    # Try Product Build first for downstream
    product_repos_endpoint = os.environ.get("PRODUCT_REPOS_ENDPOINT")
    if centos_version == "rhel9" and product_repos_endpoint:
        for arch in supported_arches[package_arch]:
            package_filename = f"{package}.{arch}.rpm"
            download_url = (
                f"{product_repos_endpoint}/latest/{centos_version}/{package_arch}/os/Packages/{package_filename}"
            )
            logging.info(f"Querying compose for : {package} in {download_url}")
            ret = download_file(
                    http_session,
                    download_url,
                    download_dir,
                )
            if ret == 0:
                logging.info("Downloaded : %s.%s", package, arch)
                return 0

    # Try compose mirrors first
    for url in mirrors:
        for repo in repos:
            for arch in supported_arches[package_arch]:
                package_filename = f"{package}.{arch}.rpm"
                download_url = (
                    f"{url}{repo}/{package_arch}/os/Packages/{package_filename}"
                )
                if "buildlogs" in url:
                    download_url = (
                        f"{url}automotive/{package_arch}/packages-main/Packages/{package_filename[0].lower()}/{package_filename}"
                    )
                logging.info(f"Querying compose for : {package} in {download_url}")
                ret = download_file(
                    http_session,
                    download_url,
                    download_dir,
                )
                if ret == 0:
                    logging.info("Downloaded : %s.%s", package, arch)
                    return 0

    #Open the package_dict
    koji_cached_lookup_table = {}
    script_path = Path(__file__).parent.resolve()
    try:
        with open(f"{script_path}/package_build_dict.json", "r") as pkg_dict:
            try:
                koji_cached_lookup_table = json.loads(pkg_dict.read())
            except Exception as e:
                koji_cached_lookup_table = {}
                logging.info("File is empty.")
                logging.error("Error: %s", str(e))

    except FileNotFoundError as e:
        logging.error("Error: file not found!")
        logging.error("Error: %s", str(e))

    # Fall back to koji/brew urls
    # eg. https://kojihub.stream.centos.org/kojifiles/packages/kernel/5.14.0/26.el9/aarch64/kernel-5.14.0-26.el9.aarch64.rpm
    for mirror in koji_mirrors:
        url, api_url = mirror.split("|", 2)
        # Have to search koji/brew for the package name
        # eg. libgcc comes from gcc
        session = koji.ClientSession(api_url, opts={"no_ssl_verify": True})
        for arch in supported_arches[package_arch]:
            # Find build of RPM
            logging.info(
                "Falling back to Koji/Brew to query for: %s.%s in %s",
                package,
                arch,
                mirror,
            )

            nvr = package.rsplit("-", 2)
            package_filename = f"{package}.{arch}.rpm"
            if nvr[0] not in koji_cached_lookup_table:
                package_name = find_koji_package(session, package, arch)
                koji_cached_lookup_table[nvr[0]] = package_name
                with open(f"{script_path}/package_build_dict.json", "w") as package_file:
                    package_file.write(json.dumps(koji_cached_lookup_table))

            package_name = koji_cached_lookup_table.get(nvr[0])
            if package_name:
                package_ver = nvr[1]
                package_rel = re.sub(r".x86_64|.aarch64|.noarch", "", nvr[2])
                # Call download_file
                ret = download_file(
                    http_session,
                    f"{url}/{package_name}/{package_ver}/{package_rel}/{arch}/{package_filename}",
                    download_dir,
                )

                if ret == 0:
                    logging.info("Downloaded : %s.%s", package, arch)
                    return 0

    logging.error(
        f"{package_arch}/{package} was not found\nURLs: {mirrors+koji_mirrors}\nRepos: {repos}"
    )
    return 1


def download_packages(
    packages_list_dir: str,
    centos_version: str,
    output_dir: str,
    arch: str,
    repos: list,
    mirrors: list,
    koji_mirrors: list,
) -> int:
    returnCode = 0

    # Create clean output directory structure
    # Failure prone so try to return nice human readable errors
    output_path = Path(output_dir)
    try:
        output_path.mkdir(parents=True, exist_ok=True)
    except Exception as e:
        logging.error("Error: Cannot create/write to %s", output_path)
        logging.error("Error: %s", str(e))
        return 1

    repo_path = output_path / arch / "os" / "Packages"
    if repo_path.exists():
        logging.info("Cleaning output directory: %s", repo_path)
        try:
            shutil.rmtree(repo_path)
        except OSError as e:
            logging.error("Error: cannot delete %s", repo_path)
            logging.error("Error: %s", str(e))
            return 1

    try:
        repo_path.mkdir(parents=True)
    except Exception as e:
        logging.error("Error: Cannot create/write to %s", repo_path)
        logging.error("Error: %s", str(e))
        return 1

    with open(
        f"{packages_list_dir}/{centos_version}-image-manifest.lock.json", "r"
    ) as packages_file:
        json_data = json.load(packages_file)

    packages = (
        json_data[f"{centos_version}"]["arch"][arch]
        + json_data[f"{centos_version}"]["common"]
    )

    http_session = requests.Session()
    http_session.mount("http://", HTTPAdapter(max_retries=10))

    results_list = []

    worker_thread = os.cpu_count() + 4
    with futures.ThreadPoolExecutor(max_workers=worker_thread) as executor:
        for package in packages:
            package = package.strip()
            results = executor.submit(
                download_package,
                package,
                output_dir,
                mirrors,
                koji_mirrors,
                repos,
                arch,
                http_session,
                centos_version
            )
            results_list.append(results)

        for idx, res in enumerate(futures.as_completed(results_list)):
            try:
                if res.result() != 0:
                    returnCode = 1
            except Exception as e:
                logging.error(
                    f"Exception {e} encountered while downloading packages"
                    f"{results_list[idx]}"
                )
                returnCode = 1

    return returnCode


def main() -> int:
    start = time.perf_counter()
    log_config_path = Path(
        os.path.join(Path(__file__).parent.absolute(), "logging.conf")
    )

    logging.config.fileConfig(log_config_path)
    logging.getLogger("manifests")
    logging.info(":: create_yum_repo started ::")

    supported_arches = ["aarch64", "x86_64"]
    repositories = ["BaseOS", "AppStream", "CRB"]

    default_repo_mirrors = [
        "http://mirror.stream.centos.org/9-stream/",
        "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/",
        "https://buildlogs.centos.org/9-stream/",
    ]

    default_koji_mirrors = [
        "https://kojihub.stream.centos.org/kojifiles/packages/|https://kojihub.stream.centos.org/kojihub"
    ]

    packages_list_dir = sys.argv[1]
    upload_prefix = sys.argv[2]
    centos_versions = sys.argv[3:]

    # The default cs9 repo mirrors can be overwritten with an env variable containing multiple
    # urls (split by spaces)
    # eg. export REPO_MIRRORS_URLS="http://xxx,http://yyy"
    repo_mirrors = os.environ.get("REPO_MIRRORS_URLS")
    if repo_mirrors:
        repo_mirrors = repo_mirrors.replace('"', "")
        repo_mirrors = repo_mirrors.split(",")
    else:
        repo_mirrors = default_repo_mirrors

    # The default cs9 koji mirrors can be overwritten with an env variable containing multiple
    # mirrors (split by a comma). Each mirror should be made up of two parts, the download base url
    # and the brew/koji base url, split with a pipe character (|).
    # eg. export KOJI_MIRRORS_URLS="http://download.xxx|http://xxx,http://download.yyy|http://yyy"

    koji_mirrors = os.environ.get("KOJI_MIRRORS_URLS")
    if koji_mirrors:
        logging.info(f"found declared KOJI_MIRRORS_URLS, using env var")
        koji_mirrors = koji_mirrors.replace('"', "")
        koji_mirrors = koji_mirrors.split(",")
    else:
        koji_mirrors = default_koji_mirrors

    release_tag = os.environ.get("GIT_RELEASE_TAG")

    aws_bucket = os.environ.get("AWS_BUCKET_REPOS")

    repo_dir = os.environ.get("REPO_DIR", "/var/lib/repos")

    repos_endpoint = os.environ.get("REPOS_ENDPOINT")

    if not os.path.isabs(repo_dir):
        logging.error(f"REPO_DIR path must be an absolute path: {repo_dir}")
        return 1

    logging.info(f"Creating repositories in: {repo_dir}")
    logging.info(f"Requested centos_versions: {centos_versions}")
    logging.info(f"release_tag (if any): {release_tag}")

    for version in centos_versions:
        logging.info(f"Creating {version} repos")
        for arch in supported_arches:
            # Download packages for this version/arch combo
            logging.info(f"Downloading packages for {version}/{arch}")
            returnCode = download_packages(
                packages_list_dir,
                version,
                f"{repo_dir}/{version}",
                arch,
                repositories,
                repo_mirrors,
                koji_mirrors,
            )
            if returnCode:
                return 1
            logging.info(f"Download finished for {version}/{arch}")

            # Generate yum repo index for downloaded packages
            returnCode = create_repo(f"{repo_dir}/{version}/{arch}/os")
            if returnCode:
                return 1

            # Test the repository on the local filesystem for this version/arch combo
            returnCode = test_local_repo(
                repo_dir=repo_dir, centos_version=version, arch=arch
            )
            if returnCode:
                return 1

        # Show the list of downloaded packages for this version
        packages_downloaded = list(Path(f"{repo_dir}/{version}").rglob("*.rpm"))
        logging.debug(f"Packages downloaded for {version}: {packages_downloaded}")
        logging.info(f"Packages downloaded for {version}: {len(packages_downloaded)}")

        # Generate index.html file for {repo_dir}/version
        create_index_html(f"{repo_dir}/{version}")

        # Upload packages for this version
        if aws_bucket:
            s3 = boto3.resource("s3")
            bucket = s3.Bucket(aws_bucket)
            upload_prefixes = [upload_prefix]

            logging.info(
                "Uploading packages (%s) to aws bucket: %s", version, aws_bucket
            )
            for prefix in upload_prefixes:
                returnCode = upload_directory(
                    directory=f"{repo_dir}/{version}",
                    prefix=f"{prefix}/{version}",
                    bucket=aws_bucket,
                )
                if returnCode:
                    return 1

                # Test all version/arch combos for this version
                for arch in supported_arches:
                    returnCode = test_remote_repo(
                        repos_endpoint=repos_endpoint,
                        upload_prefix=prefix,
                        centos_version=version,
                        arch=arch,
                    )
                    if returnCode:
                        return 1

    end = time.perf_counter()
    logging.info(f"Finished create-yum-repo in: {str(datetime.timedelta(seconds=end - start))}")

    return 0


if __name__ == "__main__":
    sys.exit(main())
