#!/bin/bash

set -euxo pipefail
# Get OS data.
source /etc/os-release
# the yum repo path
DISTRO="${OS_PREFIX}${OS_VERSION}"
# DISK_IMAGE is the target for the Makefile and the name of the resulted file.
# Example: cs9-qemu-minimal-ostree.aarch64.img
DISK_IMAGE="${DISTRO}-${TARGET}-${IMAGE_NAME}-${IMAGE_TYPE}.${ARCH}.${FORMAT}"
# .raw file is needed for import as AMI in S3
if [[ "${FORMAT}" == "img" ]]; then
  FORMAT="raw"
fi
IMAGE_FILE=${IMAGE_FILE:-"${DOWNLOAD_DIRECTORY}/${IMAGE_KEY}-${PACKAGE_SET}.${FORMAT}"}
STREAM=${STREAM:-"upstream"}
MPP_ARGS=""
MANIFEST_DIR="osbuild-manifests/osbuild-manifests"
DOWNSTREAM_DISTRO_TEMPLATE="ci/create-osbuild/downstream.yml"
DOWNSTREAM_DISTRO_FILE="${MANIFEST_DIR}/distro/${DISTRO}.ipp.yml"

workaound_for_gpg() {
  #Workaround to solve GPG check failed
  update-crypto-policies --set LEGACY
}

install_osbuild() {
  # Add Osbuild upstream yum repo to install the latest version
  curl --output /etc/yum.repos.d/osbuild-centos-stream-9.repo \
    https://copr.fedorainfracloud.org/coprs/g/osbuild/osbuild/repo/centos-stream-9/group_osbuild-osbuild-centos-stream-9.repo

  dnf -y install osbuild osbuild-tools osbuild-ostree make
}

# Create the distro file for the default mpp values for downstream
create_downstream_distro_file() {
  sed -e "s|@@DISTRO@@|${DISTRO}|g" \
      -e "s|@@DOWNSTREAM_COMPOSE_URL@@|${DOWNSTREAM_COMPOSE_URL}|g" \
      "$DOWNSTREAM_DISTRO_TEMPLATE" > "$DOWNSTREAM_DISTRO_FILE"
}

# Add options to the osbuild to enable the SSH access to the image
enable_ssh() {
  OS_OPTIONS+=("ssh_permit_root_login=true")
  OS_OPTIONS+=("extra_rpms=[\"openssh-server\",\"python3\",\"polkit\"]")
  MPP_ARGS="-D 'root_ssh_key=\"${SSH_KEY}\"'"
}

# Use the yum repo from the pipeline
override_yum_repo_with_product_build () {
  REPO_URL="${CI_REPOS_ENDPOINT}/${UUID}/${DISTRO}"
  OS_OPTIONS+=("distro_baseurl=\"${REPO_URL}\"")
  OS_OPTIONS+=("distro_automotive_repos=[]")
  OS_OPTIONS+=("distro_crb_repos=[]")
  OS_OPTIONS+=("distro_debug_repos=[]")
  OS_OPTIONS+=("distro_repos=[{\"id\":\"auto\",\"baseurl\":\"${REPO_URL}/${ARCH}/os/\"}]")
}

build_image() {
  # Set the pipeline's yum-repo
  echo "[+] Using the pipeline's yum-repo:"
  echo "repo: ${REPO_URL}"

  cd "${MANIFEST_DIR}"

  make "${DISK_IMAGE}" \
       DEFINES="${OS_OPTIONS[*]}" \
       MPP_ARGS="${MPP_ARGS}"

  echo "[+] Moving the generated image"
  mkdir -p $(dirname "$IMAGE_FILE")
  mv "$DISK_IMAGE" "$IMAGE_FILE"
}

build_metadata() {
  # record some details of the input manifest, and disk image in json
  cat <<EOF | sudo tee -a "${IMAGE_FILE%.*}".json
{
  "image_file": "${IMAGE_FILE}",
  "arch": "${ARCH}",
  "os_version": "${OS_PREFIX}${OS_VERSION}",
  "build_type": "${BUILD_TYPE}",
  "image_name": "${IMAGE_NAME}",
  "image_type": "${IMAGE_TYPE}",
  "variables": [
    {
      "UUID": "${UUID}",
      "REPO_URL": "${REPO_URL}",
      "REVISION": "${REVISION}",
      "ID": "${ID}"
    }
  ]
}
EOF
}

# Clean up
cleanup() {
  echo "[+] Cleaning up"
  make clean
}

# Use the product build repos when the package set is the product build
if [[ "${PACKAGE_SET}" == "${PRODUCT_BUILD_PREFIX}" ]]; then
  override_yum_repo_with_product_build
fi

# Use 'True' instead of 'yes' due a bug in tmt or Testing Farm 
if [[ "${TEST_IMAGE}" == "True" ]]; then
  enable_ssh
fi

if [[ "${STREAM}" == "downstream" ]]; then
  create_downstream_distro_file
fi

workaound_for_gpg

install_osbuild

build_image

build_metadata

cleanup

echo "The final image is here: ${IMAGE_FILE}"
echo "Information about image is in ${IMAGE_FILE%.*}.json"
echo
